//IMU and Magnetorquer 
//Written by Sundeep Sarwana, Allan Goodman, Dominic Chan, Grace Liu
//Last Modified 13 June 2020

//Magnetorquer Code retrieved from
//Mark Yeo; mark.yeo@student.unsw.edu.au
//Last Modified 13Apr15
//ADC implementation for controlling a magnetorquer using an L298N H-Bridge
/*
 Arduino Pinout (+ SD shield: bend out SD shield A2-A3 legs & connect to GND & 5V respectively)
 GND  - MT GND
 A0-A5 - RT clock shield (line up SCL to A5)(daisychain MN on top)
 Pin2 - LED1 + resistor to gnd (mimics Pin5) (opt.)
 Pin3 - MN READY
 Pin4 - nothing (used by SD)
 Pin5 - MT ENABLE (grey)
 Pin6 - MT IN1 (yellow + green)
 Pin7 - MT IN2 (yellow + red)
 Pin8 - LED2 + resistor to gnd (green, mimics Pin6)
 Pin9 - LED3 + resistor to gnd (red, mimics Pin7)
 Pins10-13 - nothing (used by SD)
 */

/*Notes:
 - 'counter' variable resets to -1 every time serial monitor is opened
 */


#include <Arduino.h>
#include "magnetorquer.h"
#include "MPU9250.h"
#define MT_ON_TIME 100 //length of MT pulse (ms)
#define KP 5
#define KI 0.01
#define DEADZONE 0
MPU9250 mpu;
int E1 = 6;
int M1 = 7;
int i = 0;
int j = 0;
int takeAvgRoll[] = {0,0,0,0,0};
int takeAvgPitch[] = {0,0,0,0,0};
int takeAvgYaw[] = {0,0,0,0,0};
int prev_roll = 0;
int avg_roll;
int avg_yaw;
int avg_pitch;
int prev_avg_roll = 0;
int prev_avg_yaw = 0;
int prev_avg_pitch = 0;


void setup()
{
    // Magnetorquer 1 Init
    pinMode(MT_ENABLE, OUTPUT);
    pinMode(MT_IN1, OUTPUT);
    pinMode(MT_IN2, OUTPUT);
    pinMode(MT_ENABLE_LED, OUTPUT);
    pinMode(MT_IN1_LED, OUTPUT);
    pinMode(MT_IN2_LED, OUTPUT);

    digitalWrite(MT_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT_ENABLE_LED, HIGH);
    digitalWrite(MT_IN1, LOW);
    digitalWrite(MT_IN1_LED, LOW);
    digitalWrite(MT_IN2, LOW);
    digitalWrite(MT_IN2_LED, LOW);

    // Magnetorquer 2 Init
    pinMode(MT2_ENABLE, OUTPUT);
    pinMode(MT2_IN1, OUTPUT);
    pinMode(MT2_IN2, OUTPUT);
    pinMode(MT2_ENABLE_LED, OUTPUT);
    pinMode(MT2_IN1_LED, OUTPUT);
    pinMode(MT2_IN2_LED, OUTPUT);

    digitalWrite(MT2_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT2_ENABLE_LED, HIGH);
    digitalWrite(MT2_IN1, LOW);
    digitalWrite(MT2_IN1_LED, LOW);
    digitalWrite(MT2_IN2, LOW);
    digitalWrite(MT2_IN2_LED, LOW);

    // Magnetorquer 3 Init
    pinMode(MT3_ENABLE, OUTPUT);
    pinMode(MT3_IN1, OUTPUT);
    pinMode(MT3_IN2, OUTPUT);
    pinMode(MT3_ENABLE_LED, OUTPUT);
    pinMode(MT3_IN1_LED, OUTPUT);
    pinMode(MT3_IN2_LED, OUTPUT);

    digitalWrite(MT3_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT3_ENABLE_LED, HIGH);
    digitalWrite(MT3_IN1, LOW);
    digitalWrite(MT3_IN1_LED, LOW);
    digitalWrite(MT3_IN2, LOW);
    digitalWrite(MT3_IN2_LED, LOW);

    // IMU Init
    pinMode(M1, OUTPUT);
    Serial.begin(9600);
    //115200
    Wire.begin();
    delay(2000);
    mpu.setup();
    delay(5000);

    // calibrate anytime you want to

    mpu.calibrateAccelGyro();
    mpu.calibrateMag();
    mpu.printCalibration();
}

void loop()
{
    int time = millis() - prev_ms;
    static uint32_t prev_ms = millis();
    if (time > 100) // Can be changed to increase print rate
    {
        mpu.update();
        takeAvgRoll[4] = mpu.getRoll();
        takeAvgYaw[4] = mpu.getYaw();
        takeAvgPitch[4] = mpu.getPitch();
        avg_roll = 0;
        avg_yaw = 0;
        avg_pitch = 0;
        for (int k = 0; k < 5; k++){
            avg_roll += takeAvgRoll[k];
            avg_yaw += takeAvgYaw[k];
            avg_pitch += takeAvgPitch[k];
        }

    int roll_vel = avg_roll - prev_avg_roll;
    Serial.print("AVG roll - rad/s (x-forward (north)) : ");
    Serial.println(roll_vel);

    int yaw_vel = avg_yaw - prev_avg_yaw;
    Serial.print("AVG yaw - rad/s (x-forward (north)) : ");
    Serial.println(yaw_vel);

    int pitch_vel = avg_pitch - prev_avg_pitch;
    Serial.print("AVG pitch - rad/s (x-forward (north)) : ");
    Serial.println(pitch_vel);
    
    sumOfError_roll += roll_vel*time;
    sumOfError_yaw += yaw_vel*time;
    sumOfError_pitch += pitch_vel*time;
    
    int output1 = KP*roll_vel + sumOfError_roll*KI;
    int output2 = KP*yaw_vel + sumOfError_yaw*KI;
    int output3 = KP*pitch_vel + sumOfError_pitch*KI;

    if(output1>DEADZONE){
        MT_set(output1,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (output1<-DEADZONE) {
        MT_set(output1,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }

    if(output2>DEADZONE){
        MT_set2(output2,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (output2<-DEADZONE) {
        MT_set2(output2,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }

    if(output3>DEADZONE){
        MT_set3(output3,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (output3<-DEADZONE) {
        MT_set3(output3,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }
    
    prev_avg_roll = avg_roll;
    moveDown(takeAvgRoll);
    
    prev_avg_yaw = avg_yaw;
    moveDown(takeAvgYaw);
    
    prev_avg_pitch = avg_pitch;
    moveDown(takeAvgPitch);
    
    prev_ms = millis();
    i++;
    }
}

void moveDown(int takeAvg[5]){
    for (int i = 0; i < 4; i++){
        takeAvg[i] = takeAvg[i + 1];
    }
    return;
}

int MT_set(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT_IN1, magnitude);
            analogWrite(MT_IN1_LED, magnitude);
            digitalWrite(MT_IN2, LOW);
            digitalWrite(MT_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT_IN2, magnitude);
            analogWrite(MT_IN2_LED, magnitude);
            digitalWrite(MT_IN1, LOW);
            digitalWrite(MT_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

int MT_set2(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT2_IN1, magnitude);
            analogWrite(MT2_IN1_LED, magnitude);
            digitalWrite(MT2_IN2, LOW);
            digitalWrite(MT2_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT2_IN2, magnitude);
            analogWrite(MT2_IN2_LED, magnitude);
            digitalWrite(MT2_IN1, LOW);
            digitalWrite(MT2_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

int MT_set3(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT3_IN1, magnitude);
            analogWrite(MT3_IN1_LED, magnitude);
            digitalWrite(MT3_IN2, LOW);
            digitalWrite(MT3_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT3_IN2, magnitude);
            analogWrite(MT3_IN2_LED, magnitude);
            digitalWrite(MT3_IN1, LOW);
            digitalWrite(MT3_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

void MT_stop(){
  MT_set(0, MT_FWD);
}