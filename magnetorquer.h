//Mark Yeo; mark.yeo@student.unsw.edu.au
//Last Modified 18Apr15
//ADC for controlling a single-axis magnetorquer using an L298N H-Bridge

//Pins
#define MT_ENABLE 5
#define MT_IN1 6
#define MT_IN2 7
#define MT_ENABLE_LED 2
#define MT_IN1_LED 8
#define MT_IN2_LED 9

#define MT2_ENABLE 14
#define MT2_IN1 15
#define MT2_IN2 16
#define MT2_ENABLE_LED 17
#define MT2_IN1_LED 18
#define MT2_IN2_LED 19

#define MT3_ENABLE 20
#define MT3_IN1 21
#define MT3_IN2 22
#define MT3_ENABLE_LED 23
#define MT3_IN1_LED 24
#define MT3_IN2_LED 25

#define MT_FWD 1
#define MT_REV (-1)
#define MT_MAX 255

//Initialises magnetorquer
//- returns 0 on success
int MT_init(void);

//Sets current direction and value for magnetorquer
//- dir is either FWD or REV, magnitude is between 0 and MAX inclusive
//- returns 0 on success, 1 if invalid dir, 2 if invalid magnitude
int MT_set(int dir, int magnitude);
int MT_set2(int dir, int magnitude);
int MT_set3(int dir, int magnitude);
void MT_stop();
